FROM openjdk:8-jre-alpine
VOLUME /tmp
ARG JAR_FILE
ARG BUILD_NUMBER
LABEL BUILD_NUMBER="${BUILD_NUMBER}"
RUN echo ${JAR_FILE}
ADD ${JAR_FILE} shopping-cart-service.jar
