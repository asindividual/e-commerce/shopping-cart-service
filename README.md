# shopping-cart-service


## build
```
mvn clean install
```
This command creates 2 images 1 tagged with version number and build number one as latest
Push both images to the registry
```
docker push registry.gitlab.com/asindividual/e-commerce/shopping-cart-service/development:latest
docker push registry.gitlab.com/asindividual/e-commerce/shopping-cart-service/development:<your build number to here>
```

## API descriptions
http://localhost:8186/shopping-cart-service/api/swagger-ui.html

## Single Deploy

Deployment description is placed under different repo(https://gitlab.com/asindividual/e-commerce/deployment-description) 
Locate the correct deployment folder using the service name and run following command 

```
docker-compose up -d
```

## Application Deploy
To deploy the application with all services please follow the read me file under  repository
https://gitlab.com/asindividual/e-commerce/deployment-description/-/tree/master/total-deployment/


