package name.alp.shopping.shoppingcartservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ShoppingCartRestController.class)
@Import(ShoppingCartService.class)
public class ShoppingCartServiceTest {

    @MockBean
    ShoppingCartRepository repository;
    @Autowired
    private ShoppingCartService shoppingcartService;

    @Test
    public void saveTest() {
        ShoppingCart shoppingcart = new ShoppingCart();
        shoppingcart.setId("test_id1");
        shoppingcart.setCurrentCartStatus(CartStatus.ORDER_FULFILLMENT);
        Mockito.when(repository.save(shoppingcart)).thenReturn(Mono.just(shoppingcart));

        Mono<ShoppingCart> result = shoppingcartService.save(shoppingcart);

        assertThat(result).isNotNull();
    }

    @Test
    public void findByIdTest() {
        ShoppingCart shoppingcart = new ShoppingCart();
        shoppingcart.setId("test_id1");
        shoppingcart.setCurrentCartStatus(CartStatus.ORDER_FULFILLMENT);

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(shoppingcart));

        Mono<ShoppingCart> result = shoppingcartService.findById("test_id1");
        assertThat(result).isNotNull();
    }


}
