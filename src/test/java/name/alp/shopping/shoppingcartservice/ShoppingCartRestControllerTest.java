package name.alp.shopping.shoppingcartservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ShoppingCartRestController.class)
@Import(ShoppingCartService.class)
public class ShoppingCartRestControllerTest {

    @MockBean
    ShoppingCartRepository repository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        ShoppingCart shoppingcart = new ShoppingCart();
        shoppingcart.setId("test_id1");
        shoppingcart.setCurrentCartStatus(CartStatus.ORDER_FULFILLMENT);


        Mockito.when(repository.save(shoppingcart)).thenReturn(Mono.just(shoppingcart));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(shoppingcart))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById() {
        ShoppingCart shoppingcart = new ShoppingCart();
        shoppingcart.setId("test_id1");
        shoppingcart.setCurrentCartStatus(CartStatus.ORDER_FULFILLMENT);

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(shoppingcart));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.currentCartStatus").isEqualTo(CartStatus.ORDER_FULFILLMENT.toString())
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }


}
