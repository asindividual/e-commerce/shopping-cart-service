package name.alp.shopping.shoppingcartservice.client.category;

import name.alp.shopping.shoppingcartservice.client.BaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;


@Service
public class CategoryClient extends BaseClient<Category> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryClient.class);

    public CategoryClient(DiscoveryClient client,  @Value("${feign.services.category-service.baseUrl}") String categoryServiceBaseUrl ) {
        super(client, "category-service",categoryServiceBaseUrl);
    }
}
