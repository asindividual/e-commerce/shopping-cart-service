package name.alp.shopping.shoppingcartservice.client.products;

import name.alp.shopping.shoppingcartservice.client.BaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;


@Service
public class ProductsClient extends BaseClient<Product> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsClient.class);

    public ProductsClient(DiscoveryClient client,  @Value("${feign.services.product-service.baseUrl}") String productServiceBaseUrl ) {
        super(client, "product-service",productServiceBaseUrl);
    }
}
