package name.alp.shopping.shoppingcartservice;

import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ShoppingCartService {

    private final ShoppingCartRepository shoppingcartRepository;

    public ShoppingCartService(ShoppingCartRepository shoppingcartRepository) {
        this.shoppingcartRepository = shoppingcartRepository;
    }

    public Mono<ShoppingCart> save(ShoppingCart shoppingcart) {
        if (shoppingcart.getId() == null) {
            shoppingcart.setDateCreated(new Date());
            shoppingcart.setCurrentCartStatus(CartStatus.ORDER_FULFILLMENT);
        }
        return shoppingcartRepository.save(shoppingcart);
    }

    public Mono<ShoppingCart> findById(String id) {
        return shoppingcartRepository.findById(id);
    }


    public Mono<ShoppingCart> patch(ShoppingCart shoppingcart) {
        Mono<ShoppingCart> coupon = shoppingcartRepository.findById(shoppingcart.getId());
        AtomicReference<ShoppingCart> atomicCoupon = new AtomicReference<>();
        Disposable subscribe = coupon.subscribe(shoppingcartDBO -> {
            atomicCoupon.set(shoppingcartDBO);
        });

        block(subscribe);

        ShoppingCart existingCart = atomicCoupon.get();
        if (shoppingcart.getCurrentCartStatus() != null) {
            existingCart.setCurrentCartStatus(shoppingcart.getCurrentCartStatus());
        }
        if (shoppingcart.getAppliedCampaigns() != null) {
            existingCart.setAppliedCampaigns(shoppingcart.getAppliedCampaigns());
        }
        if (shoppingcart.getAppliedCoupons() != null) {
            existingCart.setAppliedCoupons(shoppingcart.getAppliedCoupons());
        }
        if (shoppingcart.getCalculatedCost() != null) {
            existingCart.setCalculatedCost(shoppingcart.getCalculatedCost());
        }
        if (shoppingcart.getProductInstances() != null) {
            existingCart.setProductInstances(shoppingcart.getProductInstances());
        }
        if (shoppingcart.getSelectedLogisticsCompany() != null) {
            existingCart.setSelectedLogisticsCompany(shoppingcart.getSelectedLogisticsCompany());
        }
        existingCart.setDateUpdated(new Date());
        return save(existingCart);
    }

    private void block(Disposable subscribe) {
        while (!subscribe.isDisposed()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
