package name.alp.shopping.shoppingcartservice;

import name.alp.shopping.shoppingcartservice.client.products.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Document("shopping_cart")
public class ShoppingCart {
    @Id
    private String id;
    private List<ProductInstance> productInstances;
    private List<String> appliedCampaigns;
    private List<String> appliedCoupons;
    private Double calculatedCost;
    private String selectedLogisticsCompany;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private java.util.Date dateCreated;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private java.util.Date dateUpdated;
    private CartStatus currentCartStatus;


    public ShoppingCart() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ProductInstance> getProductInstances() {
        return productInstances;
    }

    public void setProductInstances(List<ProductInstance> productInstances) {
        this.productInstances = productInstances;
    }

    public List<String> getAppliedCampaigns() {
        return appliedCampaigns;
    }

    public void setAppliedCampaigns(List<String> appliedCampaigns) {
        this.appliedCampaigns = appliedCampaigns;
    }

    public List<String> getAppliedCoupons() {
        return appliedCoupons;
    }

    public void setAppliedCoupons(List<String> appliedCoupons) {
        this.appliedCoupons = appliedCoupons;
    }

    public Double getCalculatedCost() {
        return calculatedCost;
    }

    public void setCalculatedCost(Double calculatedCost) {
        this.calculatedCost = calculatedCost;
    }

    public String getSelectedLogisticsCompany() {
        return selectedLogisticsCompany;
    }

    public void setSelectedLogisticsCompany(String selectedLogisticsCompany) {
        this.selectedLogisticsCompany = selectedLogisticsCompany;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public CartStatus getCurrentCartStatus() {
        return currentCartStatus;
    }

    public void setCurrentCartStatus(CartStatus currentCartStatus) {
        this.currentCartStatus = currentCartStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart shoppingcart = (ShoppingCart) o;
        return id.equals(shoppingcart.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
