package name.alp.shopping.shoppingcartservice;

import name.alp.shopping.shoppingcartservice.client.products.Product;

public class ProductInstance {
    private Product product;
    private Integer amount;

    public ProductInstance() {

    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
