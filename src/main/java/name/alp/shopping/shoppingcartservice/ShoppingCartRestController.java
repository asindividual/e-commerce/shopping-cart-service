package name.alp.shopping.shoppingcartservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
public class ShoppingCartRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingCartRestController.class);
    private final ShoppingCartService shoppingcartService;

    public ShoppingCartRestController(ShoppingCartService shoppingcartService) {
        this.shoppingcartService = shoppingcartService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates")
    @PostMapping
    public Mono<ShoppingCart> save(@RequestBody ShoppingCart shoppingcart) {
        LOGGER.info("save: {}", shoppingcart);
        return shoppingcartService.save(shoppingcart);
    }

    @Operation(summary = "Provides an update for all non-null values in the argument",
            description = "This endpoint uses id of the provided argument")
    @PatchMapping
    public Mono<ShoppingCart> patch(@RequestBody ShoppingCart shoppingcart) {
        LOGGER.info("patch: {}", shoppingcart);
        return shoppingcartService.patch(shoppingcart);
    }

    @Operation(summary = "Provides an entity for the specified id")
    @GetMapping("/{id}")
    public Mono<ShoppingCart> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return shoppingcartService.findById(id);
    }


}
