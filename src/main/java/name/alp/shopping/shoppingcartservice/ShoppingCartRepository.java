package name.alp.shopping.shoppingcartservice;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingCartRepository extends ReactiveMongoRepository<ShoppingCart, String> {

}
