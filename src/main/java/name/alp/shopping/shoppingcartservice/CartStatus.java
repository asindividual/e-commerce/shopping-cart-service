package name.alp.shopping.shoppingcartservice;

public enum CartStatus {
    ORDER_FULFILLMENT,
    PAYMENT_PROCESSING,
    PICKING_AND_PACKING,
    SHIPPING_AND_DELIVERY
}
